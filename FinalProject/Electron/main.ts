const { app, BrowserWindow } = require('electron')

function createWindow () {
  // Creates the browser window.
  let win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // Loads the index.html of the app.
  win.loadFile('./assets/index.html')
}

app.on('ready', createWindow)