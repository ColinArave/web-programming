var _a = require('electron'), app = _a.app, BrowserWindow = _a.BrowserWindow;
function createWindow() {
    // Creates the browser window.
    var win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });
    // Loads the index.html of the app.
    win.loadFile('./assets/index.html');
}
app.on('ready', createWindow);
